/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.pos.client;

import edu.ifpb.pos.Pessoa;
import java.util.Collection;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Ricardo Job
 */
public class ExemploLinhaComando {

    public static void main(String[] args) {

        

        ConsumidorREST cliente = new ConsumidorREST();
        System.out.println("-- Antes --");
        listar(cliente.listarPessoa());
        System.out.println("-- Salvar --");
        salvar(cliente);
        System.out.println("-- Depois --");
        listar(cliente.listarPessoa());
    }

    private static void salvar(ConsumidorREST cliente) throws ClientErrorException {
        Pessoa novaPessoa = new Pessoa(36, "Nhonnho");
        cliente.salvar(novaPessoa);
    }

    private static void listar(Collection<Pessoa> lista) throws ClientErrorException {
        for (Pessoa pessoa : lista) {
            System.out.println(pessoa);
        }
    }
}
