package edu.ifpb.pos.client;

import edu.ifpb.pos.Pessoa;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 * @author Ricardo Job
 */
public class ConsumidorREST {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/ExemploREST/cadastro";

    public ConsumidorREST() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("pessoa");
    }

    public List<Pessoa> listarPessoa() throws ClientErrorException {

        GenericType<List<Pessoa>> customerType = new GenericType<List<Pessoa>>() {
        };

        WebTarget resource = webTarget;

        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(customerType);
    }

    public void salvar(Object requestEntity) throws ClientErrorException {

        webTarget.request(MediaType.APPLICATION_XML).
                post(Entity.xml(requestEntity),
                        Pessoa.class);
    }

    public void close() {
        client.close();
    }

}
