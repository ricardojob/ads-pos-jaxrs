package edu.ifpb.dac.jsf;

import edu.ifpb.dac.Pessoa;
import edu.ifpb.dac.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "controlador")
@SessionScoped
public class PessoaController implements Serializable {

    private Pessoa pessoa = new Pessoa();

   private ClienteExemplo consumidor = new ClienteExemplo();

    private List<Pessoa> pessoas = new ArrayList<>();
//    @EJB
//    private CadastroPessoa ejbFacade;

    public PessoaController() {
        pessoas = consumidor.listarPessoa();
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String salvar() {
        try {
            consumidor.salvar(pessoa);
            JsfUtil.addSuccessMessage("Pessoa Salva Com Sucesso!!");
            pessoa = new Pessoa();
            pessoas = consumidor.listarPessoa();
            return null;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Erro ao salvar");
            return null;
        }
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

}
