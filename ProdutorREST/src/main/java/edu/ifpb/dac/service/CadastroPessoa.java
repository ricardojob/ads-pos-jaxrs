/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.service;

import edu.ifpb.dac.Pessoa;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ricardo Job
 */
@Local
public interface CadastroPessoa {

    public void create(Pessoa pessoa);

    public void edit(Pessoa pessoa);

    public void remove(Pessoa pessoa);

    public Pessoa find(Object id);

    public List<Pessoa> findAll();

    public List<Pessoa> findRange(int[] range);

    public int count();

}
